from datetime import datetime
from app import db



#Construction_type
class ConstructionType(db.Model):
    __tablename__ = 'construction_types'
    id = db.Column(db.Integer, primary_key=True)
    construction_name = db.Column(db.String(64))
    construction_image = db.Column(db.String(64))
    constructions = db.relationship('Construction', backref='construction_types', lazy='dynamic',
                                    cascade='all, delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Construction_type {}>'.format(self.id)


#construction
class Construction(db.Model):
    __tablename__ = 'constructions'
    id = db.Column(db.Integer, primary_key=True)
    construction_name = db.Column(db.String(64))
    construction_price = db.Column(db.String(64))
    construction_image = db.Column(db.String(64))
    construction_location = db.Column(db.String(64))
    construction_types_id = db.Column(db.Integer, db.ForeignKey('construction_types.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Construction {}>'.format(self.id)
