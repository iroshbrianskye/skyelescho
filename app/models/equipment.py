from datetime import datetime
from app import db


#Equipment_type
class EquipmentType(db.Model):
    __tablename__ = "equipment_types"
    id = db.Column(db.Integer, primary_key=True)
    equipment_name  = db.Column(db.String(64))
    equipment_image = db.Column(db.String(64))
    equipments = db.relationship('Equipment', backref='equipment_types', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Equipment_type {}>'.format(self.id)


#equipment
class Equipment(db.Model):
    __tablename__ = "equipments"
    id = db.Column(db.Integer, primary_key=True)
    equipment_name = db.Column(db.String(64))
    equipment_price = db.Column(db.String(64))
    equipment_image = db.Column(db.String(64))
    equipment_location = db.Column(db.String(64))
    equipment_type_id = db.Column(db.Integer, db.ForeignKey('equipment_types.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Equipment {}>'.format(self.id)
