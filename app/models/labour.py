from datetime import datetime
from app import db


#Labour_type
class LabourType(db.Model):
    __tablename__ = 'labour_types'
    id = db.Column(db.Integer, primary_key=True)
    labour_name  = db.Column(db.String(64))
    labour_image = db.Column(db.String(64))
    labours = db.relationship('Labour', backref='labour_types', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Labour_type {}>'.format(self.id)


#labour
class Labour(db.Model):
    __tablename__ = 'labours'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    image = db.Column(db.String(64))
    county = db.Column(db.String(64))
    town = db.Column(db.String(64))
    dob = db.Column(db.DateTime)
    gender = db.Column(db.String(64))
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    labour_type_id = db.Column(db.Integer, db.ForeignKey('labour_types.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Labour {}>'.format(self.id)
