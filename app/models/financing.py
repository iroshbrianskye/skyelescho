from datetime import datetime
from app import db


#financing
class Financing(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    financing_name  = db.Column(db.String(64))
    financing_description = db.Column(db.String(120))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<financing {}>'.format(self.id)
