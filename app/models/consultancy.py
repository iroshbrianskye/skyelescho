from datetime import datetime
from app import db


#Consultancy_type
class ConsultancyType(db.Model):
    __tablename__ = 'consultancy_types'
    id = db.Column(db.Integer, primary_key=True)
    consultancy_name = db.Column(db.String(64))
    consultancy_image = db.Column(db.String(64))
    consultancies = db.relationship('Consultancy', backref='consultancy_types', lazy='dynamic',
                                    cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Consultancy_type {}>'.format(self.id)


#consultancy
class Consultancy(db.Model):
    __tablename__ = 'consultancies'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    image = db.Column(db.String(64))
    county = db.Column(db.String(64))
    town = db.Column(db.String(64))
    dob = db.Column(db.DateTime)
    gender = db.Column(db.String(64))
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    consultancy_type_id = db.Column(db.Integer, db.ForeignKey('consultancy_types.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Consultancy {}>'.format(self.id)
