from datetime import datetime
from app import db


#House_designs_type
class HouseDesignType(db.Model):
    __tablename__ = 'house_design_types'
    id = db.Column(db.Integer, primary_key=True)
    house_design_name = db.Column(db.String(64))
    house_design_image = db.Column(db.String(64))
    house_designs = db.relationship('HouseDesign', backref='house_design_types', lazy='dynamic',
                                    cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<House_design_type {}>'.format(self.id)


#house_design
class HouseDesign(db.Model):
    __tablename__ = 'house_designs'
    id = db.Column(db.Integer, primary_key=True)
    house_design_name = db.Column(db.String(64))
    house_design_image = db.Column(db.String(64))
    house_design_id = db.Column(db.Integer, db.ForeignKey('house_design_types.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<House_design_type {}>'.format(self.id)
