from datetime import datetime
from app import db


#project
class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    project_name  = db.Column(db.String(64))
    project_client  = db.Column(db.String(64))
    project_image  = db.Column(db.String(64))
    project_start_date = db.Column(db.DateTime, index=True)
    project_end_date = db.Column(db.DateTime, index=True)
    project_description = db.Column(db.String(120))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<project {}>'.format(self.id)

    def to_dict_project(self):
        data = {
            'id': self.id,
            'project_name': self.project_name,
            'project_client': self.project_client,
            'project_image': self.project_image,
            'project_start_date': self.project_start_date,
            'project_end_date': self.project_end_date,
            'project_description': self.project_description,
        }
        return data
