from datetime import datetime
from app import db
from numpy import genfromtxt
from collections import OrderedDict


#Property_type
class PropertyType(db.Model):
    __tablename__ = 'property_types'
    id = db.Column(db.Integer, primary_key=True)
    property_name = db.Column(db.String(64))
    property_image = db.Column(db.String(64))
    properties = db.relationship('Property', backref='property_types', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Property_type {}>'.format(self.id)


#property
class Property(db.Model):
    __tablename__ = 'properties'
    id = db.Column(db.Integer, primary_key=True)
    property_name = db.Column(db.String(64))
    property_price = db.Column(db.Numeric)
    property_image = db.Column(db.String(64))
    property_location = db.Column(db.String(64))
    property_mode = db.Column(db.String(64))
    property_vacancy = db.Column(db.Integer)
    property_type_id = db.Column(db.Integer, db.ForeignKey('property_types.id'))
    county_id = db.Column(db.Integer, db.ForeignKey('county.id'))
    property_features = db.relationship('PropertyFeature', backref='property', lazy='dynamic',cascade='all,delete-orphan')
    tenants = db.relationship('Tenant', backref='house', lazy='dynamic',cascade='all,delete-orphan')
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Property {}>'.format(self.id)


#property_features
class PropertyFeature(db.Model):
    __tablename__ = 'property_features'
    id = db.Column(db.Integer, primary_key=True)
    feature_name = db.Column(db.String(64))
    feature_icon = db.Column(db.String(64))
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))

    def __repr__(self):
        return '<property_features {}>'.format(self.id)

#county
class County(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    SNo = db.Column(db.Integer)
    county = db.Column(db.String(64))
    properties = db.relationship('Property', backref='county', lazy='dynamic', cascade='all,delete-orphan')

    def __repr__(self):
        return '<county {}>'.format(self.id)

    def Load_Data(file_name):
        data = genfromtxt(file_name,dtype={'names':('SNo','county'),'formats':('i4','U15')},delimiter=',', skip_header=1)
        return data.tolist()

class Tenant(db.Model):
    __tablename__ = 'tenants'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    image = db.Column(db.String(64))
    county = db.Column(db.String(64))
    town = db.Column(db.String(64))
    dob = db.Column(db.DateTime)
    gender = db.Column(db.String(64))
    rent_due = db.Column(db.Float, default=0)
    rent_updated_at=db.Column(db.DateTime, index=True, default=datetime.utcnow)
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    house_no = db.Column(db.Integer)
    payments = db.relationship('Payment', backref='tenant', lazy='dynamic', cascade='all,delete-orphan')
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Labour {}>'.format(self.id)

#payment
class Payment(db.Model):
    MODE  = OrderedDict([('cash', 'Cash'),('cheque', 'Cheque'),('online-payment', 'Online-Payment')])
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.String(140))
    amount = db.Column(db.Float)
    Tenant = db.Column(db.Integer, db.ForeignKey('tenants.id'))
    payment_date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    payment_mode =db.Column(db.Enum(*MODE, name='payment_mode', native_enum=False), index=True, nullable=True, server_default='cash')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Payment {}>'.format(self.id)
