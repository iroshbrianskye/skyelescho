from datetime import datetime, timedelta
from hashlib import md5
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from collections import OrderedDict
import jwt
import json
from app import db, login
import redis
import rq
import base64
import os,random
from xkcdpass import xkcd_password as xp

#roles
class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(64))
    access_property = db.Column(db.Boolean, default=False)
    access_construction = db.Column(db.Boolean, default=False)
    access_consultancy = db.Column(db.Boolean, default=False)
    access_equipment = db.Column(db.Boolean, default=False)
    access_labour = db.Column(db.Boolean, default=False)
    access_house_design = db.Column(db.Boolean, default=False)
    access_financing = db.Column(db.Boolean, default=False)
    access_projects = db.Column(db.Boolean, default=False)
    access_user = db.Column(db.Boolean, default=False)
    permissions = db.relationship('Permission', backref='role', uselist=False, cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user = db.relationship('User', backref='role', uselist=False, cascade='all,delete-orphan')

    def __repr__(self):
        return '<Role {}>'.format(self.id)

    def to_dict_role(self):
        data = {
            'id': self.id,
            'access_property': self.access_property,
            'access_construction': self.access_construction,
            'access_consultancy': self.access_consultancy,
            'access_equipment': self.access_equipment,
            'access_labour': self.access_labour,
            'access_house_design': self.access_house_design,
            'access_financing': self.access_financing,
            'access_projects': self.access_projects,
            'access_user': self.access_user,
            'read_property': self.permissions.read_property,
            'read_construction': self.permissions.read_construction,
            'read_consultancy': self.permissions.read_consultancy,
            'read_equipment': self.permissions.read_equipment,
            'read_labour': self.permissions.read_labour,
            'read_house_design': self.permissions.read_house_design,
            'read_financing': self.permissions.read_financing,
            'read_projects': self.permissions.read_projects,
            'read_user': self.permissions.read_user,
            'write_property': self.permissions.write_property,
            'write_construction': self.permissions.write_construction,
            'write_consultancy': self.permissions.write_consultancy,
            'write_equipment': self.permissions.write_equipment,
            'write_labour': self.permissions.write_labour,
            'write_house_design': self.permissions.write_house_design,
            'write_financing': self.permissions.write_financing,
            'write_projects': self.permissions.write_projects,
            'write_user': self.permissions.write_user,
            'create_property': self.permissions.create_property,
            'create_construction': self.permissions.create_construction,
            'create_consultancy': self.permissions.create_consultancy,
            'create_equipment': self.permissions.create_equipment,
            'create_labour': self.permissions.create_labour,
            'create_house_design': self.permissions.create_house_design,
            'create_financing': self.permissions.create_financing,
            'create_projects': self.permissions.create_projects,
            'create_user': self.permissions.create_user,
            'delete_property': self.permissions.delete_property,
            'delete_construction': self.permissions.delete_construction,
            'delete_consultancy': self.permissions.delete_consultancy,
            'delete_equipment': self.permissions.delete_equipment,
            'delete_labour': self.permissions.delete_labour,
            'delete_house_design': self.permissions.delete_house_design,
            'delete_financing': self.permissions.delete_financing,
            'delete_projects': self.permissions.delete_projects,
            'delete_user': self.permissions.delete_user
        }
        return data

    def from_dict(self, data):
        for field in ['access_property','access_construction','access_consultancy','access_equipment','access_labour','access_house_design','access_financing','access_projects','access_user']:
            if field in data:
                if data[field] == 'false':
                    setattr(self, field, False)
                elif data[field] == 'true':
                    setattr(self, field, True)

    def from_dict_first_registration(self):
        for field in ['access_property','access_construction','access_consultancy','access_equipment','access_labour','access_house_design','access_financing','access_projects','access_user']:
            setattr(self, field, True)

#user login
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    role_id=db.Column(db.Integer, db.ForeignKey('role.id'))
    author = db.relationship('Property', backref='author',lazy='dynamic',cascade='all,delete-orphan')
    tenants = db.relationship('Tenant', backref='author',lazy='dynamic',cascade='all,delete-orphan')
    payments = db.relationship('Payment', backref='author',lazy='dynamic',cascade='all,delete-orphan')
    phone_number = db.Column(db.String(120))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def capitalize_first_letter(self,s):
        new_str = []
        s = s.split(" ")
        for i, c in enumerate(s):
            new_str.append(c.capitalize())
        return "".join(new_str)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

#permissions
class Permission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    read_property = db.Column(db.Boolean, default=False)
    read_construction = db.Column(db.Boolean, default=False)
    read_consultancy = db.Column(db.Boolean, default=False)
    read_equipment = db.Column(db.Boolean, default=False)
    read_labour = db.Column(db.Boolean, default=False)
    read_house_design = db.Column(db.Boolean, default=False)
    read_financing = db.Column(db.Boolean, default=False)
    read_projects = db.Column(db.Boolean, default=False)
    read_user = db.Column(db.Boolean, default=False)
    write_property = db.Column(db.Boolean, default=False)
    write_construction = db.Column(db.Boolean, default=False)
    write_consultancy = db.Column(db.Boolean, default=False)
    write_equipment = db.Column(db.Boolean, default=False)
    write_labour = db.Column(db.Boolean, default=False)
    write_house_design = db.Column(db.Boolean, default=False)
    write_financing = db.Column(db.Boolean, default=False)
    write_projects = db.Column(db.Boolean, default=False)
    write_user = db.Column(db.Boolean, default=False)
    create_property = db.Column(db.Boolean, default=False)
    create_construction = db.Column(db.Boolean, default=False)
    create_consultancy = db.Column(db.Boolean, default=False)
    create_equipment = db.Column(db.Boolean, default=False)
    create_labour = db.Column(db.Boolean, default=False)
    create_house_design = db.Column(db.Boolean, default=False)
    create_financing = db.Column(db.Boolean, default=False)
    create_projects = db.Column(db.Boolean, default=False)
    create_user = db.Column(db.Boolean, default=False)
    delete_property = db.Column(db.Boolean, default=False)
    delete_construction = db.Column(db.Boolean, default=False)
    delete_consultancy = db.Column(db.Boolean, default=False)
    delete_equipment = db.Column(db.Boolean, default=False)
    delete_labour = db.Column(db.Boolean, default=False)
    delete_house_design = db.Column(db.Boolean, default=False)
    delete_financing = db.Column(db.Boolean, default=False)
    delete_projects = db.Column(db.Boolean, default=False)
    delete_user = db.Column(db.Boolean, default=False)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Role {}>'.format(self.id)

    def from_dict_permissions(self, data):
        for field in ['read_property','read_construction','read_consultancy','read_equipment','read_labour','read_house_design','read_financing','read_projects','read_user','write_property',
                      'write_construction','write_consultancy','write_equipment','write_labour','write_house_design','write_financing','write_projects','write_user','create_property',
                      'create_construction','create_consultancy','create_equipment','create_labour','create_house_design','create_financing','create_projects','create_user','delete_property',
                      'delete_construction','delete_consultancy','delete_equipment','delete_labour','delete_house_design','delete_financing','delete_projects','delete_user']:
            if field in data:
                if data[field] == 'false':
                    setattr(self, field, False)
                elif data[field] == 'true':
                    setattr(self, field, True)

    def from_dict_permissions_first_registration(self):
        for field in ['read_property','read_construction','read_consultancy','read_equipment','read_labour','read_house_design','read_financing','read_projects','read_user','write_property',
                      'write_construction','write_consultancy','write_equipment','write_labour','write_house_design','write_financing','write_projects','write_user','create_property',
                      'create_construction','create_consultancy','create_equipment','create_labour','create_house_design','create_financing','create_projects','create_user','delete_property',
                      'delete_construction','delete_consultancy','delete_equipment','delete_labour','delete_house_design','delete_financing','delete_projects','delete_user']:
            setattr(self, field, True)

#company settings
class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name    = db.Column(db.String(64))
    contact_person  = db.Column(db.String(64))
    address         = db.Column(db.String(120))
    country         = db.Column(db.String(64))
    city            = db.Column(db.String(64))
    state           = db.Column(db.String(64))
    postal_code     = db.Column(db.String(64))
    email           = db.Column(db.String(64), index=True)
    phone_number    = db.Column(db.Integer, index=True)
    bank_name       = db.Column(db.String(64))
    bank_country    = db.Column(db.String(64))
    bank_address    = db.Column(db.String(120))
    account_number  = db.Column(db.Integer, index=True)

    def __repr__(self):
        return '<Company {}>'.format(self.id)

#theme settings
class Theme(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name    = db.Column(db.String(64))
    logo            = db.Column(db.String(64))
    favicon         = db.Column(db.String(120))

    def __repr__(self):
        return '<Theme {}>'.format(self.id)
