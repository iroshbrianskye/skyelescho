from datetime import datetime
from flask import render_template, flash, redirect, abort, url_for, request, g, jsonify, current_app
from flask_login import current_user, login_required
from flask_uploads import UploadSet, configure_uploads, IMAGES
from app import db
from flask import Blueprint
from app.models import *
from app.home import bp
from app.home.forms import SearchForm,RegistrationForm

photos = UploadSet('photos', IMAGES)

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.company = Company.query.first()
    g.images = Theme.query.first()


@bp.route('/')
@bp.route('/index')
def index():
    propertylist = Property.query.filter(Property.property_vacancy > 0).order_by(Property.created_at.desc()).all()
    locationlist = Property.query.order_by(Property.created_at.desc()).group_by(Property.property_location)
    types = Property.query.order_by(Property.created_at.desc()).group_by(Property.property_type_id)
    maxval =  int(max(node.property_price for node in propertylist)) if propertylist else  None
    search = {'locationlist': locationlist, 'types': types, 'maxval': maxval}
    return render_template('home/index.html',search=search, propertylist=propertylist)

@bp.route('/properties_search')
def properties_search():
    propertylist = Property.query.filter(Property.property_vacancy > 0).order_by(Property.created_at.desc()).all()
    locationlist = Property.query.order_by(Property.created_at.desc()).group_by(Property.property_location)
    types = Property.query.order_by(Property.created_at.desc()).group_by(Property.property_type_id)
    maxval =  int(max(node.property_price for node in propertylist)) if propertylist else  None
    search = {'locationlist': locationlist, 'types': types, 'maxval': maxval}
    if request.args.get('location'):
        location = request.args.get('location')
        property_type = request.args.get('property-types')
        min_price = request.args.get('min_price')
        max_price = request.args.get('max_price')
        if location == 'Location' and property_type == 'Property Types':
            propertylist = Property.query.filter(Property.property_price >=min_price).filter(Property.property_vacancy > 0).filter(Property.property_price <= max_price).order_by(Property.created_at.desc()).all()
        elif location != 'Location' and property_type == 'Property Types':
            propertylist = Property.query.filter(Property.property_price >=min_price).filter(Property.property_vacancy > 0).filter(Property.property_price <= max_price).filter( \
                            Property.property_location.like('%' + location + '%')).order_by(Property.created_at.desc()).all()
        elif location == 'Location' and property_type != 'Property Types':
            propertylist = Property.query.filter(Property.property_price >=min_price).filter(Property.property_vacancy > 0).filter(Property.property_price <= max_price).filter( \
                            Property.property_type_id == property_type).order_by(Property.created_at.desc()).all()
        elif location != 'Location' and property_type != 'Property Types':
            propertylist = Property.query.filter(Property.property_price >=min_price).filter(Property.property_vacancy > 0).filter(Property.property_price <= max_price).filter( \
                            Property.property_type_id == property_type).filter(Property.property_location.like('%' + location + '%')).order_by(Property.created_at.desc()).all()
    return render_template('home/properties-search.html',search=search, propertylist=propertylist)

@bp.route('/about_us')
def about_us():
    return render_template('home/aboutus.html')

@bp.route('/our_services')
def our_services():
    return render_template('home/ourservices.html')

@bp.route('/lescho_foundation')
def lescho_foundation():
    return render_template('home/leschofoundation.html')

@bp.route('/contact_us')
def contact_us():
    return render_template('home/contactus.html')

@bp.route('/property_details')
def property_details():
    return render_template('home/property-details.html')

@bp.route('/getproperty')
def getproperty():
    id = request.args.get('id')
    property = Property.query.filter_by(id=id).first_or_404()
    data = {'name': property.property_name,
            'image': property.property_image,
            'price': float(property.property_price),
            'location': property.property_location,
            'county': property.county.county,
            'mode': property.property_mode,
            'vacancy': property.property_vacancy,
            'feature_name':[feature.feature_name for feature in property.property_features],
            'feature_icon':[feature.feature_icon for feature in property.property_features]
            }
    return jsonify(data=data)

@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    form.role.choices = [(row.id, row.role) for row in Role.query.filter(Role.id!=1).filter(Role.role!='tenant').order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user = User(username=form.username.data, phone_number=form.phone_number.data, role=role, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!', 'success')
        return redirect(url_for('auth.login'))
    return render_template('home/register.html', title=('Register'),
                           form=form)
