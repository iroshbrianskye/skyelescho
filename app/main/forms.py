# uncompyle6 version 3.2.3
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.15rc1 (default, Apr 15 2018, 21:51:34)
# [GCC 7.3.0]
# Embedded file name: /run/user/1000/gvfs/smb-share:server=dibro-elite.local,share=pycharmprojects/lesho-construction/app/main/forms.py
# Compiled at: 2018-09-11 16:54:09
from flask_wtf import FlaskForm
from wtforms import Form as NoCsrfForm
from wtforms.fields import BooleanField, StringField, SubmitField, FloatField, PasswordField, IntegerField, DateField, SelectField, FieldList, TextAreaField, FormField, HiddenField, DecimalField, FloatField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError
from app.models import *
from flask_login import current_user
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_babel import _, lazy_gettext as _l
from flask import request, g
from sqlalchemy import and_
photos = UploadSet('photos', IMAGES)

class ConstructionTypeForm(FlaskForm):
    construction_name = StringField('Construction Name', validators=[DataRequired()])
    construction_image = FileField('Construction Image')
    submit = SubmitField('Save')


class ConstructionForm(FlaskForm):
    construction_name = StringField('Construction Name', validators=[DataRequired()])
    construction_price = StringField('Construction Price', validators=[DataRequired()])
    construction_image = FileField('Construction Image')
    construction_location = StringField('Construction Location', validators=[DataRequired()])
    submit = SubmitField('Save')


class ConsultancyTypeForm(FlaskForm):
    consultancy_name = StringField('Consultancy Name', validators=[DataRequired()])
    consultancy_image = FileField('Image')
    submit = SubmitField('Save')


class ConsultancyForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Second Name', validators=[DataRequired()])
    image = FileField('Image')
    county = StringField('County', validators=[DataRequired()])
    town = StringField('Town', validators=[DataRequired()])
    dob = DateField('Date of Birth', validators=[DataRequired()], format='%d/%m/%Y')
    gender = SelectField('Gender', choices=[('male', 'Male'), ('female', 'Female')])
    phone_number = StringField('Phone Number', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Save')


class EquipmentTypeForm(FlaskForm):
    equipment_name = StringField('Name', validators=[DataRequired()])
    equipment_image = FileField('Image')
    submit = SubmitField('Save')


class EquipmentForm(FlaskForm):
    equipment_name = StringField('Name', validators=[DataRequired()])
    equipment_price = StringField('Price', validators=[DataRequired()])
    equipment_image = FileField('Image')
    equipment_location = StringField('First Name', validators=[DataRequired()])
    submit = SubmitField('Save')


class FinancingForm(FlaskForm):
    financing_name = StringField('Name', validators=[DataRequired()])
    financing_description = TextAreaField('Description', validators=[DataRequired()])
    submit = SubmitField('Save')


class HouseDesignTypeForm(FlaskForm):
    house_design_name = StringField('Name', validators=[DataRequired()])
    house_design_image = FileField('Image')
    submit = SubmitField('Save')


class HouseDesignForm(FlaskForm):
    house_design_name = StringField('Name', validators=[DataRequired()])
    house_design_image = FileField('Image')
    submit = SubmitField('Save')


class LabourTypeForm(FlaskForm):
    labour_name = StringField('Name', validators=[DataRequired()])
    labour_image = FileField('Image')
    submit = SubmitField('Save')


class LabourForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Second Name', validators=[DataRequired()])
    image = FileField('Labour Image')
    county = StringField('County', validators=[DataRequired()])
    town = StringField('Town', validators=[DataRequired()])
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')
    gender = SelectField('Gender', choices=[('male', 'Male'), ('female', 'Female')])
    phone_number = StringField('Phone Number', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Save')

class TenantForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Second Name', validators=[DataRequired()])
    image = FileField('Tenant Image')
    county = StringField('County', validators=[DataRequired()])
    town = StringField('Town', validators=[DataRequired()])
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')
    gender = SelectField('Gender', choices=[('male', 'Male'), ('female', 'Female')])
    phone_number = StringField('Phone Number', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    house = SelectField('house',coerce=int)
    house_no = IntegerField('house_no')
    submit = SubmitField('Save')

    def validate_house(self, house):
        property = Property.query.filter_by(id=house.data).first_or_404()
        if property.property_vacancy == 0:
            raise ValidationError(_('All vacancies filled for this house'))
        return

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

    def validate_house_no(self, house_no):
        tenant = Tenant.query.filter(and_(Tenant.property_id == self.house.data, Tenant.house_no == house_no.data)).first()
        if tenant is not None:
            raise ValidationError(_('House already has a tenant'))
        return

class EditTenantForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Second Name', validators=[DataRequired()])
    image = FileField('Tenant Image')
    county = StringField('County', validators=[DataRequired()])
    town = StringField('Town', validators=[DataRequired()])
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')
    gender = SelectField('Gender', choices=[('male', 'Male'), ('female', 'Female')])
    phone_number = StringField('Phone Number', validators=[DataRequired()])
    house = SelectField('house',coerce=int)
    house_no = IntegerField('house_no')
    submit = SubmitField('Save')

    def validate_house_no(self, house_no):
        if house_no.data != g.house_no or self.house.data !=g.house_id:
            tenant = Tenant.query.filter(and_(Tenant.property_id == self.house.data, Tenant.house_no == house_no.data)).first()
            if tenant is not None:
                raise ValidationError(_('House already has a tenant'))


class ProjectForm(FlaskForm):
    project_name = StringField('Project Name', validators=[DataRequired()], id='project_name')
    project_client = StringField('Client Name', validators=[DataRequired()], id='project_client')
    project_image = FileField('Project Image', validators=[FileAllowed(photos, u'Image only!')], id='project_image')
    project_start_date = DateField('Start Date', format='%d/%m/%Y', validators=[DataRequired()], id='project_start_date')
    project_end_date = DateField('End Date', format='%d/%m/%Y', validators=[DataRequired()], id='project_end_date')
    project_description = TextAreaField('Description', validators=[DataRequired()], id='project_description')
    submit = SubmitField('Save')


class PropertyTypeForm(FlaskForm):
    property_name = StringField('Property Name', validators=[DataRequired()])
    property_image = FileField('Property Type Image')
    submit = SubmitField('Save')


class PropertyFeatureForm(NoCsrfForm):
    feature_name = StringField('Name', validators=[DataRequired()])
    feature_icon = StringField('Feature icon', validators=[DataRequired()])


class PropertyForm(FlaskForm):
    property_name = StringField('Property Name', validators=[DataRequired()])
    property_price =DecimalField('Price', validators=[DataRequired()])
    property_image = FileField(validators=[FileAllowed(photos, u'Image only!')])
    property_county = SelectField('county',coerce=int)
    property_mode = SelectField('mode', choices=[('rent', 'For Rent'), ('sale', 'For Sale')])
    property_vacancy = IntegerField('Property Vacancy', validators=[DataRequired()])
    property_location = StringField('Location', validators=[DataRequired()])
    submit = SubmitField('Save')


class EditPasswordForm(FlaskForm):
    oldpassword = PasswordField('Password', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(),
     EqualTo('password')])
    submit = SubmitField('Save')

    def validate_oldpassword(self, oldpassword):
        user = User.query.filter_by(id=current_user.id).first()
        if user is None or not user.check_password(oldpassword.data):
            raise ValidationError('Please enter correct Password.')
        return


class RegistrationForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    email = StringField(_l('Email'), validators=[DataRequired(), Email()])
    phone_number = StringField(_l('phone_number'), validators=[DataRequired()])
    role = SelectField('role', coerce=int)
    password = PasswordField(_l('Password'), validators=[DataRequired()])
    password2 = PasswordField(_l('Repeat Password'), validators=[DataRequired(),
     EqualTo('password')])
    submit = SubmitField(_l('Save'))

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError(_('Please use a different email address.'))
        return


class EditRegistrationForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    phone_number = StringField(_l('phone_number'), validators=[DataRequired()])
    role = SelectField('role', coerce=int)
    submit = SubmitField(_l('Save'))


class CompanyForm(FlaskForm):
    company_name = StringField('company_name', validators=[DataRequired()])
    contact_person = StringField('contact_person', validators=[DataRequired()])
    address = StringField('address', validators=[DataRequired()])
    country = StringField('country', validators=[DataRequired()])
    city = StringField('city', validators=[DataRequired()])
    state = StringField('state', validators=[DataRequired()])
    postal_code = StringField('postal_code', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    phone_number = IntegerField('phone_number', validators=[DataRequired()])
    bank_name = StringField('bank_name', validators=[DataRequired()])
    bank_country = StringField('bank_country', validators=[DataRequired()])
    bank_address = StringField('bank_address', validators=[DataRequired()])
    account_number = IntegerField('account_number', validators=[DataRequired()])
    submit = SubmitField('Save & update')


class ThemeForm(FlaskForm):
    company_name = StringField('company_name', validators=[DataRequired()])
    logo = FileField(validators=[FileAllowed(photos, u'Image only!'), FileRequired(u'File was empty!')])
    favicon = FileField(validators=[FileAllowed(photos, u'Image only!'), FileRequired(u'File was empty!')])
    submit = SubmitField(u'Save')


class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])
    table = HiddenField(_l('table'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)

class PaymentForm(FlaskForm):
    tenant_id        = SelectField('tenant_id',coerce=int, id='select_tenant')
    payment_mode      = SelectField('payment_mode',choices=[('cash', 'cash'), ('cheque', 'cheque'), ('online-payment', 'online payment')])
    amount            = FloatField('amount', validators=[DataRequired()])
    note              = TextAreaField('note', validators=[DataRequired()])
    submit            = SubmitField('save')
