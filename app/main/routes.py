# uncompyle6 version 3.2.3
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.15rc1 (default, Apr 15 2018, 21:51:34)
# [GCC 7.3.0]
# Embedded file name: /run/user/1000/gvfs/smb-share:server=dibro-elite.local,share=pycharmprojects/lesho-construction/app/main/routes.py
# Compiled at: 2018-09-11 17:46:42
from datetime import datetime
from flask import render_template, flash, redirect, abort, url_for, request, g, jsonify, current_app
from flask_login import current_user, login_required
from flask_uploads import UploadSet, configure_uploads, IMAGES
from app import db
from flask import Blueprint
from app.models import *
from app.main.forms import *
from xkcdpass import xkcd_password as xp
import os, random, string
from app.auth.email import send_user_added_email
main = Blueprint('main', __name__)
photos = UploadSet('photos', IMAGES)


@main.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.company = Company.query.first()
    g.images = Theme.query.first()
    now = datetime.utcnow()
    if(now.day == 5):
        tenants=Tenant.query.all()
        updated_at=tenants[0].rent_updated_at
        if updated_at.month != now.month:
            for tenant in tenants:
                tenant.rent_due=(float(tenant.house.property_price) + tenant.rent_due)
                tenant.rent_updated_at=datetime.utcnow()
            db.session.commit()
    county=County.query.first()
    if county is None:
        file_name = 'conties.csv'
        data = County.Load_Data(file_name)
        for i in data:
            record = County(**{
                'SNo' : i[0],
                'county' : i[1]
            })
            db.session.add(record)
        db.session.commit()



@main.route('/', methods=['GET', 'POST'])
@main.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    property_count = Property.query.filter_by(author=current_user).count()
    if current_user.role.role == 'admin':
        property_count = Property.query.count()
    tenant=None
    if current_user.role.role == 'tenant':
        tenant = Tenant.query.filter_by(email=current_user.email).first_or_404()
    labour_count = Labour.query.count()
    equipment_count = Equipment.query.count()
    consultancy_count = Consultancy.query.count()
    count_all = {'property': property_count, 'labour': labour_count, 'equipment': equipment_count,
                 'consultancy': consultancy_count,'tenant':tenant}
    labour = Labour.query.limit(5)
    consultancy = Consultancy.query.limit(5)
    property = Property.query.filter_by(author=current_user).limit(5)
    if current_user.role.role == 'admin':
        property = Property.query.limit(5)
    equipment = Equipment.query.limit(5)
    tables = {'labour': labour, 'consultancy': consultancy, 'property': property, 'equipment': equipment}
    return render_template('admin/index.html', count_all=count_all, tables=tables)


@main.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    if current_user.role.access_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    form.role.choices = [(row.id, row.role) for row in Role.query.order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user = User(username=form.username.data, phone_number=form.phone_number.data, role=role, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User added successfully!', 'success')
        return redirect(url_for('main.users'))
    users = User.query.order_by(User.last_seen.desc()).all()
    return render_template('admin/users.html', form=form, users=users)


@main.route('/edit_user/<id>', methods=['GET', 'POST'])
@login_required
def edit_user(id):
    if current_user.role.permissions.write_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=id).first_or_404()
    form = EditRegistrationForm(obj=user)
    form.role.choices = [(row.id, row.role) for row in Role.query.order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user.username = form.username.data
        user.role = role
        user.phone_number = form.phone_number.data
        db.session.commit()
        flash('User edited successfully', 'success')
        return redirect(url_for('main.users'))
    return render_template('admin/edit_user.html', form=form, title='Edit User')


@main.route('/add_role', methods=['GET', 'POST'])
@login_required
def add_role():
    if request.method == 'POST':
        role = Role(role=request.form['role'])
        permission = Permission(role=role)
        db.session.add(role)
        db.session.commit()
    return jsonify({'status': 1})

#edit role
@main.route('/edit_role/<id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    role = Role.query.filter_by(id=id).first_or_404()
    if request.method == 'POST':
        role.role = request.form['role']
        db.session.commit()
        return jsonify({'status': 1})
    elif request.method == 'GET':
        data={'role':role.role, 'id':role.id}
        return jsonify({'status': 1, 'data':data})


@main.route('/view_user/<id>')
@login_required
def view_user(id):
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_user.html', title='view user', user=user)


@main.route('/role_permissions', methods=['GET', 'POST'])
@login_required
def role_permissions():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    roles = Role.query.order_by(Role.created_at.desc()).all()
    return render_template('admin/role_permissions.html', roles=roles, title='Roles & Permission')


@main.route('/role_permissions_values', methods=['GET', 'POST'])
@login_required
def role_permissions_values():
    role = Role.query.order_by(Role.created_at.desc()).first()
    if role is None:
        return jsonify({'status': 0})
    if request.method == 'POST':
        role = Role.query.filter_by(id=request.form['id']).first_or_404()
    response = jsonify({'status': 1, 'data': role.to_dict_role()})
    return response


@main.route('/edit_role_permissions_js', methods=['GET', 'POST'])
@login_required
def edit_role_permissions_js():
    role = Role.query.filter_by(id=request.form['id']).first_or_404()
    data = request.form
    role.from_dict(data)
    role.permissions.from_dict_permissions(data)
    db.session.commit()
    return jsonify({'status': 1})


@main.route('/company_settings', methods=['GET', 'POST'])
@login_required
def company_settings():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    company = Company.query.first()
    if company is None:
        form = CompanyForm()
        if form.validate_on_submit():
            company = Company(company_name=form.company_name.data, contact_person=form.contact_person.data,
                              phone_number=form.phone_number.data, email=form.email.data, address=form.address.data,
                              country=form.country.data, city=form.city.data, state=form.state.data,
                              postal_code=form.postal_code.data, bank_name=form.bank_name.data,
                              bank_country=form.bank_country.data, bank_address=form.bank_address.data,
                              account_number=form.account_number.data)
            db.session.add(company)
            db.session.commit()
            flash('company settings updated successfully', 'success')
            return redirect(url_for('main.company_settings'))
    else:
        form = CompanyForm(obj=company)
        if form.validate_on_submit():
            form.populate_obj(company)
            db.session.commit()
            flash('company settings updated successfully', 'success')
            return redirect(url_for('main.company_settings'))
    return render_template('admin/company_settings.html', form=form, title='Company Settings')


@main.route('/theme_settings', methods=['GET', 'POST'])
@login_required
def theme_settings():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    theme = Theme.query.first()
    if theme is None:
        form = ThemeForm()
        if form.validate_on_submit():
            photos = UploadSet('photos', IMAGES)
            logo = photos.save(form.logo.data)
            favicon = photos.save(form.favicon.data)
            theme = Theme(company_name=form.company_name.data, logo=logo, favicon=favicon)
            db.session.add(theme)
            db.session.commit()
            flash('theme settings updated successfully', 'success')
            return redirect(url_for('main.theme_settings'))
    else:
        form = ThemeForm(obj=theme)
        if form.validate_on_submit():
            photos = UploadSet('photos', IMAGES)
            logo = photos.save(form.logo.data)
            favicon = photos.save(form.favicon.data)
            theme.company_name = form.company_name.data
            theme.logo = logo
            theme.favicon = favicon
            db.session.commit()
            flash('theme settings updated successfully', 'success')
            return redirect(url_for('main.theme_settings'))
    return render_template('admin/theme_settings.html', form=form, title='Theme Settings')


@main.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = EditPasswordForm()
    if form.validate_on_submit():
        current_user.set_password(form.password.data)
        db.session.commit()
        flash('Password changed successfully', 'success')
        return redirect(url_for('main.settings'))
    return render_template('admin/settings.html', form=form, title='Edit Password')


@main.route('/view_property_type', methods=['GET', 'POST'])
@login_required
def view_property_type():
    if current_user.role.access_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'PropertyType'
    page = request.args.get('page', 1, type=int)
    property_types = PropertyType.query.order_by(PropertyType.created_at.desc()).paginate(page, current_app.config[
        'POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_property_type', page=property_types.next_num) if property_types.has_next else None
    prev_url = url_for('main.view_property_type', page=property_types.prev_num) if property_types.has_prev else None
    return render_template('admin/property.html', property_types=property_types.items, next_url=next_url, prev_url=prev_url,
                           title='property Types')


@main.route('/view_labour_type', methods=['GET', 'POST'])
@login_required
def view_labour_type():
    if current_user.role.access_labour != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'LabourType'
    page = request.args.get('page', 1, type=int)
    labour_types = LabourType.query.order_by(LabourType.created_at.desc()).paginate(page, current_app.config[
        'POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_labour_type', page=labour_types.next_num) if labour_types.has_next else None
    prev_url = url_for('main.view_labour_type', page=labour_types.prev_num) if labour_types.has_prev else None
    return render_template('admin/labour.html', labour_types=labour_types.items, next_url=next_url, prev_url=prev_url,
                           title='labour Types')


@main.route('/view_equipment_type', methods=['GET', 'POST'])
@login_required
def view_equipment_type():
    if current_user.role.access_equipment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'EquipmentType'
    page = request.args.get('page', 1, type=int)
    equipment_types = EquipmentType.query.order_by(EquipmentType.created_at.desc()).paginate(page, current_app.config[
        'POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_equipment_type', page=equipment_types.next_num) if equipment_types.has_next else None
    prev_url = url_for('main.view_equipment_type', page=equipment_types.prev_num) if equipment_types.has_prev else None
    return render_template('admin/equipment.html', equipment_types=equipment_types.items, next_url=next_url,
                           prev_url=prev_url, title='equipment Types')


@main.route('/view_construction_materials_type', methods=['GET', 'POST'])
@login_required
def view_construction_materials_type():
    if current_user.role.access_construction != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'ConstructionType'
    page = request.args.get('page', 1, type=int)
    construction_types = ConstructionType.query.order_by(ConstructionType.created_at.desc()).paginate(page,
                                                                                                      current_app.config[
                                                                                                          'POSTS_PER_PAGE'],
                                                                                                      False)
    next_url = url_for('main.view_construction_materials_type', page=construction_types.next_num) if construction_types.has_next else None
    prev_url = url_for('main.view_construction_materials_type', page=construction_types.prev_num) if construction_types.has_prev else None
    return render_template('admin/construction-materials.html', construction_types=construction_types.items,
                           next_url=next_url, prev_url=prev_url, title='construction materials Types')


@main.route('/view_consultancy_services_type', methods=['GET', 'POST'])
@login_required
def view_consultancy_services_type():
    if current_user.role.access_consultancy != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'ConsultancyType'
    page = request.args.get('page', 1, type=int)
    consultancy_types = ConsultancyType.query.order_by(ConsultancyType.created_at.desc()).paginate(page,
                                                                                                   current_app.config[
                                                                                                       'POSTS_PER_PAGE'],
                                                                                                   False)
    next_url = url_for('main.view_consultancy_services_type', page=consultancy_types.next_num) if consultancy_types.has_next else None
    prev_url = url_for('main.view_consultancy_services_type', page=consultancy_types.prev_num) if consultancy_types.has_prev else None
    return render_template('admin/consultancy-services.html', consultancy_types=consultancy_types.items, next_url=next_url,
                           prev_url=prev_url, title='consultancy Types')


@main.route('/view_latest_house_designs_type', methods=['GET', 'POST'])
@login_required
def view_latest_house_designs_type():
    if current_user.role.access_house_design != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'HouseDesignType'
    page = request.args.get('page', 1, type=int)
    house_designs_types = HouseDesignType.query.order_by(HouseDesignType.created_at.desc()).paginate(page,
                                                                                                     current_app.config[
                                                                                                         'POSTS_PER_PAGE'],
                                                                                                     False)
    next_url = url_for('main.view_latest_house_designs_type', page=house_designs_types.next_num) if house_designs_types.has_next else None
    prev_url = url_for('main.view_latest_house_designs_type', page=house_designs_types.prev_num) if house_designs_types.has_prev else None
    return render_template('admin/latest-house-designs.html', house_designs_types=house_designs_types.items,
                           next_url=next_url, prev_url=prev_url, title='latest house designs Types')


@main.route('/financing', methods=['GET', 'POST'])
@login_required
def financing():
    if current_user.role.access_financing != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Financing'
    form = FinancingForm()
    page = request.args.get('page', 1, type=int)
    financing = Financing.query.order_by(Financing.created_at.desc()).paginate(page,
                                                                               current_app.config['POSTS_PER_PAGE'],
                                                                               False)
    next_url = url_for('main.financing', page=financing.next_num) if financing.has_next else None
    prev_url = url_for('main.financing', page=financing.prev_num) if financing.has_prev else None
    return render_template('admin/financing.html', financing=financing.items, next_url=next_url, prev_url=prev_url, form=form,
                           title='financing')


@main.route('/projects', methods=['GET', 'POST'])
@login_required
def projects():
    if current_user.role.access_projects != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ProjectForm()
    g.search_form.table.data = 'Project'
    page = request.args.get('page', 1, type=int)
    projects = Project.query.order_by(Project.created_at.desc()).paginate(page, current_app.config['POSTS_PER_PAGE'],
                                                                          False)
    next_url = url_for('main.projects', page=projects.next_num) if projects.has_next else None
    prev_url = url_for('main.projects', page=projects.prev_num) if projects.has_prev else None
    return render_template('admin/projects.html', projects=projects.items, next_url=next_url, prev_url=prev_url, form=form,
                           title='projects')

@main.route('/tenants', methods=['GET', 'POST'])
@login_required
def tenants():
    if current_user.role.access_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    tenants = Tenant.query.filter_by(author=current_user).order_by(Tenant.created_at.desc()).all()
    if current_user.role.role == 'admin':
        tenants = Tenant.query.order_by(Tenant.created_at.desc()).all()
    return render_template('admin/tenants.html', tenants=tenants, title='tenants')

@main.route('/property_features', methods=['GET', 'POST'])
@login_required
def property_features():
    return render_template('admin/property-features.html', title='property-features')

#payments
@main.route('/payments')
@login_required
def payments():
    if current_user.role.access_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payments = Payment.query.filter_by(author=current_user).order_by(Payment.created_at.desc()).all()
    if current_user.role.role == 'admin':
        payments = Payment.query.order_by(Payment.created_at.desc()).all()
    if current_user.role.role == 'tenant':
        tenant = Tenant.query.filter_by(email=current_user.email).first_or_404()
        payments = Payment.query.filter_by(tenant=tenant).order_by(Payment.created_at.desc()).all()
    return render_template('admin/payments.html', title = 'Payment', payments=payments)


@main.route('/<property_type_id>/add_property', methods=['GET', 'POST'])
@login_required
def add_property(property_type_id):
    if current_user.role.permissions.create_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    id = property_type_id
    property_type_id = PropertyType.query.filter_by(id=property_type_id).first_or_404()
    form = PropertyForm()
    form.property_county.choices = [(row.id, row.county) for row in County.query.all()]
    if form.validate_on_submit():
        property_image = form.property_image.data
        if property_image is not None:
            property_image = photos.save(form.property_image.data)
        county=County.query.filter_by(id=form.property_county.data).first_or_404()
        newproperty = Property(property_name=form.property_name.data, property_price=form.property_price.data,
                               property_location=form.property_location.data, property_image=property_image,
                               property_types=property_type_id,county=county,property_vacancy=form.property_vacancy.data, property_mode=form.property_mode.data, author=current_user)
        db.session.add(newproperty)
        db.session.commit()
        flash('Property added successfully', 'success')
        return redirect(url_for('main.view_property', id=id))
    return render_template('admin/add-property.html', form=form, title='add property')


@main.route('/<labour_type_name>/add_labour', methods=['GET', 'POST'])
@login_required
def add_labour(labour_type_name):
    if current_user.role.permissions.create_labour != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = LabourForm()
    labour_type_id = LabourType.query.filter_by(labour_name=labour_type_name).first_or_404()
    if form.validate_on_submit():
        labour_image = form.image.data
        if labour_image is not None:
            labour_image = photos.save(form.image.data)
        newlabour = Labour(first_name=form.first_name.data, last_name=form.last_name.data, image=labour_image,
                           county=form.county.data, town=form.town.data, dob=form.dob.data, gender=form.gender.data,
                           phone_number=form.phone_number.data, email=form.email.data, labour_types=labour_type_id)
        db.session.add(newlabour)
        db.session.commit()
        flash('Labour added successfully', 'success')
        return redirect(url_for('main.view_labour', labour_name=labour_type_name))
    return render_template('admin/add-labour.html', form=form, title='add labour')

@main.route('/add_tenant', methods=['GET', 'POST'])
@login_required
def add_tenant():
    if current_user.role.permissions.create_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = TenantForm()
    form.house.choices = [(row.id, row.property_name) for row in Property.query.filter(Property.property_vacancy > 0).filter(Property.author==current_user).filter(Property.property_mode=='rent').order_by(Property.created_at.desc()).all()]
    if form.validate_on_submit():
        property_id = Property.query.filter_by(id=form.house.data).first_or_404()
        tenant_image = form.image.data
        if tenant_image is not None:
            tenant_image = photos.save(form.image.data)
        property_id.property_vacancy=(property_id.property_vacancy - 1)
        newtenant = Tenant(first_name=form.first_name.data, last_name=form.last_name.data, image=tenant_image,
                           county=form.county.data, town=form.town.data, dob=form.dob.data, gender=form.gender.data,
                           phone_number=form.phone_number.data, email=form.email.data,rent_due=property_id.property_price, house=property_id, house_no=form.house_no.data,author=current_user)
        words = xp.locate_wordfile()
        mywords = xp.generate_wordlist(wordfile=words, min_length=5, max_length=8)
        raw_password = xp.generate_xkcdpassword(mywords)
        role=Role.query.filter_by(role='tenant').first_or_404()
        user = User(username=form.first_name.data,phone_number=form.phone_number.data, role=role, email=form.email.data)
        user.set_password(current_user.capitalize_first_letter(raw_password))
        send_user_added_email(user,current_user.capitalize_first_letter(raw_password))
        db.session.add(user)
        db.session.commit()
        flash('Tenant added successfully', 'success')
        return redirect(url_for('main.tenants'))
    return render_template('admin/add-tenant.html', form=form, title='add tenant')

#add payment
@main.route('/add_payment', methods=['GET', 'POST'])
@login_required
def add_payment():
    if current_user.role.permissions.create_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = PaymentForm()
    form.tenant_id.choices = [(row.id, row.email) for row in Tenant.query.filter_by(author=current_user).order_by(Tenant.created_at.desc()).all()]
    if form.validate_on_submit():
        tenant = Tenant.query.filter_by(id=form.tenant_id.data).first_or_404()
        tenant.rent_due=(tenant.rent_due-form.amount.data )
        payment = Payment(payment_mode=form.payment_mode.data,note=form.note.data, amount=form.amount.data,tenant=tenant,author=current_user)
        db.session.commit()
        flash('Payment added successfully', 'success')
        return redirect(url_for('main.payments'))
    return render_template('admin/add-payment.html', title="add Payment", form=form)


@main.route('/<equipment_type_name>/add_equipment', methods=['GET', 'POST'])
@login_required
def add_equipment(equipment_type_name):
    if current_user.role.permissions.create_equipment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = EquipmentForm()
    equipment_type_id = EquipmentType.query.filter_by(equipment_name=equipment_type_name).first_or_404()
    if form.validate_on_submit():
        equipment_image = form.equipment_image.data
        if equipment_image is not None:
            equipment_image = photos.save(form.equipment_image.data)
        newequipment = Equipment(equipment_name=form.equipment_name.data, equipment_price=form.equipment_price.data,
                                 equipment_location=form.equipment_location.data, equipment_image=equipment_image,
                                 equipment_types=equipment_type_id)
        db.session.add(newequipment)
        db.session.commit()
        flash('Equipment added successfully', 'success')
        return redirect(url_for('main.view_equipment', equipment_name=equipment_type_name))
    return render_template('admin/add-equipment.html', form=form, title='add equipment')


@main.route('/<construction_type_name>/add_construction_materials', methods=['GET', 'POST'])
@login_required
def add_construction_materials(construction_type_name):
    if current_user.role.permissions.create_construction != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ConstructionForm()
    construction_type_id = ConstructionType.query.filter_by(construction_name=construction_type_name).first_or_404()
    if form.validate_on_submit():
        construction_image = form.construction_image.data
        if construction_image is not None:
            construction_image = photos.save(form.construction_image.data)
        newconstruction = Construction(construction_name=form.construction_name.data,
                                       construction_price=form.construction_price.data,
                                       construction_location=form.construction_location.data,
                                       construction_image=construction_image, construction_types=construction_type_id)
        db.session.add(newconstruction)
        db.session.commit()
        flash('Construction added successfully', 'success')
        return redirect(url_for('main.view_construction_materials', construction_name=construction_type_name))
    return render_template('admin/add-construction-materials.html', form=form, title='add construction_materials')


@main.route('/<consultancy_type_name>/add_consultancy_services', methods=['GET', 'POST'])
@login_required
def add_consultancy_services(consultancy_type_name):
    if current_user.role.permissions.create_consultancy != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ConsultancyForm()
    consultancy_type_id = ConsultancyType.query.filter_by(consultancy_name=consultancy_type_name).first_or_404()
    if form.validate_on_submit():
        consultancy_image = form.image.data
        if consultancy_image is not None:
            consultancy_image = photos.save(form.image.data)
        newconsultancy = Consultancy(first_name=form.first_name.data, last_name=form.last_name.data,
                                     image=consultancy_image, county=form.county.data, town=form.town.data,
                                     dob=form.dob.data, gender=form.gender.data, phone_number=form.phone_number.data,
                                     email=form.email.data, consultancy_types=consultancy_type_id)
        db.session.add(newconsultancy)
        db.session.commit()
        flash('Consultancy added successfully', 'success')
        return redirect(url_for('main.view_consultancy_services', consultancy_name=consultancy_type_name))
    return render_template('admin/add-consultancy-services.html', form=form, title='add consultancy_services')


@main.route('/<house_design_type_name>/add_latest_house_designs', methods=['GET', 'POST'])
@login_required
def add_latest_house_designs(house_design_type_name):
    if current_user.role.permissions.create_house_design != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = HouseDesignForm()
    house_design_type_id = HouseDesignType.query.filter_by(house_design_name=house_design_type_name).first_or_404()
    if form.validate_on_submit():
        house_design_image = form.house_design_image.data
        if house_design_image is not None:
            house_design_image = photos.save(form.house_design_image.data)
        newhouse_design = HouseDesign(house_design_name=form.house_design_name.data,
                                      house_design_image=house_design_image, house_design_types=house_design_type_id)
        db.session.add(newhouse_design)
        db.session.commit()
        flash('House Design added successfully', 'success')
        return redirect(url_for('main.view_latest_house_designs', house_design_name=house_design_type_name))
    return render_template('admin/add-latest-house-designs.html', form=form, title='add latest_house_designs')


@main.route('/add_financing', methods=['POST'])
@login_required
def add_financing():
    if current_user.role.permissions.create_financing != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = FinancingForm()
    if form.validate_on_submit():
        financing = Financing(financing_name=form.financing_name.data,
                              financing_description=form.financing_description.data)
        db.session.add(financing)
        db.session.commit()
        return jsonify({'status': 1, 'message': 'finance added successfully'})
    return jsonify(data=form.errors)


@main.route('/add_projects', methods=['GET', 'POST'])
@login_required
def add_projects():
    if current_user.role.permissions.create_projects != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ProjectForm()
    if form.validate_on_submit():
        project_image = form.project_image.data
        if project_image is not None:
            project_image = photos.save(form.project_image.data)
        project = Project(project_name=form.project_name.data, project_description=form.project_description.data,
                          project_image=project_image, project_client=form.project_client.data,
                          project_start_date=form.project_start_date.data, project_end_date=form.project_end_date.data)
        db.session.add(project)
        db.session.commit()
        return jsonify({'status': 1, 'message': 'project added successfully'})
    return jsonify(data=form.errors)


@main.route('/add_property_features/<id>', methods=['GET', 'POST'])
@login_required
def add_property_features(id):
    if current_user.role.permissions.create_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    if request.method == 'POST':
        property = Property.query.filter_by(id=id).first_or_404()
        feature = PropertyFeature(feature_icon=request.form['feature_icon'], feature_name=request.form['feature_name'],
                                  property=property)
        db.session.add(feature)
        db.session.commit()
        return jsonify({'status': 1, 'message': 'feature edited successfully'})
    return jsonify(status=0)


@main.route('/add_property_type', methods=['GET', 'POST'])
def add_property_type():
    if current_user.role.permissions.create_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = PropertyTypeForm()
    if form.validate_on_submit():
        property_image = form.property_image.data
        if property_image is not None:
            property_image = photos.save(form.property_image.data)
        propertytype = PropertyType(property_name=form.property_name.data, property_image=property_image)
        db.session.add(propertytype)
        db.session.commit()
        flash('Property Type successfully created', 'success')
        return redirect(url_for('main.view_property_type'))
    return render_template('admin/add-property-type.html', form=form, title='add property type')


@main.route('/add_labour_type', methods=['GET', 'POST'])
@login_required
def add_labour_type():
    if current_user.role.permissions.create_labour != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = LabourTypeForm()
    if form.validate_on_submit():
        labour_image = form.labour_image.data
        if labour_image is not None:
            labour_image = photos.save(form.labour_image.data)
        labourtype = LabourType(labour_name=form.labour_name.data, labour_image=labour_image)
        db.session.add(labourtype)
        db.session.commit()
        flash('Labour Type added successfully', 'success')
        return redirect(url_for('main.view_labour_type'))
    return render_template('admin/add-labour-type.html', form=form, title='add labour type')


@main.route('/add_equipment_type', methods=['GET', 'POST'])
@login_required
def add_equipment_type():
    if current_user.role.permissions.create_equipment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = EquipmentTypeForm()
    if form.validate_on_submit():
        equipment_image = form.equipment_image.data
        if equipment_image is not None:
            equipment_image = photos.save(form.equipment_image.data)
        equipmenttype = EquipmentType(equipment_name=form.equipment_name.data, equipment_image=equipment_image)
        db.session.add(equipmenttype)
        db.session.commit()
        flash('Equipment Type added successfully', 'success')
        return redirect(url_for('main.view_equipment_type'))
    return render_template('admin/add-equipment-type.html', form=form, title='add equipment type')


@main.route('/add_construction_materials_type', methods=['GET', 'POST'])
@login_required
def add_construction_materials_type():
    if current_user.role.permissions.create_construction != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ConstructionTypeForm()
    if form.validate_on_submit():
        construction_image = form.construction_image.data
        if construction_image is not None:
            construction_image = photos.save(form.construction_image.data)
        constructionmaterialtype = ConstructionType(construction_name=form.construction_name.data,
                                                    construction_image=construction_image)
        db.session.add(constructionmaterialtype)
        db.session.commit()
        flash('Construction Type added successfully', 'success')
        return redirect(url_for('main.view_construction_materials_type'))
    return render_template('admin/add-construction-materials-type.html', form=form, title='add construction_materials type')


@main.route('/add_consultancy_services_type', methods=['GET', 'POST'])
@login_required
def add_consultancy_services_type():
    if current_user.role.permissions.create_consultancy != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ConsultancyTypeForm()
    if form.validate_on_submit():
        consultancy_image = form.consultancy_image.data
        if consultancy_image is not None:
            consultancy_image = photos.save(form.consultancy_image.data)
        consultancyservicetype = ConsultancyType(consultancy_name=form.consultancy_name.data,
                                                 consultancy_image=consultancy_image)
        db.session.add(consultancyservicetype)
        db.session.commit()
        flash('Consultancy Type added successfully', 'success')
        return redirect(url_for('main.view_consultancy_services_type'))
    return render_template('admin/add-consultancy-services-type.html', form=form, title='add consultancy_services type')


@main.route('/add_latest_house_designs_type', methods=['GET', 'POST'])
@login_required
def add_latest_house_designs_type():
    if current_user.role.permissions.create_house_design != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = HouseDesignTypeForm()
    if form.validate_on_submit():
        house_design_image = form.house_design_image.data
        if house_design_image is not None:
            house_design_image = photos.save(form.house_design_image.data)
        housedesigntype = HouseDesignType(house_design_name=form.house_design_name.data,
                                          house_design_image=house_design_image)
        db.session.add(housedesigntype)
        db.session.commit()
        flash('House Design type added successfully', 'success')
        return redirect(url_for('main.view_latest_house_designs_type'))
    return render_template('admin/add-latest-house-designs-type.html', form=form, title='add latest_house_designs type')


@main.route('/edit_property/<id>', methods=['GET', 'POST'])
@login_required
def edit_property(id):
    if current_user.role.permissions.write_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    property = Property.query.filter_by(id=id).first_or_404()
    form = PropertyForm(obj=property)
    form.property_county.choices = [(row.id, row.county) for row in County.query.all()]
    if form.validate_on_submit():
        property_image = form.property_image.data
        if property_image is not None and property.property_image != property_image:
            property_image = photos.save(form.property_image.data)
            form.property_image.data = property_image
        form.populate_obj(property)
        db.session.commit()
        flash('Property  edited ', 'success')
        return redirect(url_for('main.view_property', id=property.property_types.id))
    return render_template('admin/edit-property.html', property=property, form=form, title='edit property')


@main.route('/edit_labour/<id>', methods=['GET', 'POST'])
@login_required
def edit_labour(id):
    if current_user.role.permissions.write_labour != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    labour = Labour.query.filter_by(id=id).first_or_404()
    form = LabourForm(obj=labour)
    if form.validate_on_submit():
        labour_image = form.image.data
        if labour_image is not None and labour.image != labour_image:
            labour_image = photos.save(form.image.data)
            form.image.data = labour_image
        form.populate_obj(labour)
        db.session.commit()
        flash('Labour  edited ', 'success')
        return redirect(url_for('main.view_labour', labour_name=labour.labour_types.labour_name))
    return render_template('admin/edit-labour.html', labour=labour, form=form, title='edit labour type')


@main.route('/edit_equipment/<id>', methods=['GET', 'POST'])
@login_required
def edit_equipment(id):
    if current_user.role.permissions.write_equipment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    equipment = Equipment.query.filter_by(id=id).first_or_404()
    form = EquipmentForm(obj=equipment)
    if form.validate_on_submit():
        equipment_image = form.equipment_image.data
        if equipment_image is not None and equipment.equipment_image != equipment_image:
            equipment_image = photos.save(form.equipment_image.data)
            form.equipment_image.data = equipment_image
        form.populate_obj(equipment)
        db.session.commit()
        flash('Equipment  edited ', 'success')
        return redirect(url_for('main.view_equipment', equipment_name=equipment.equipment_types.equipment_name))
    return render_template('admin/edit-equipment.html', equipment=equipment, form=form, title='edit equipment')


@main.route('/edit_construction_materials/<id>', methods=['GET', 'POST'])
@login_required
def edit_construction_materials(id):
    if current_user.role.permissions.write_construction != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    construction = Construction.query.filter_by(id=id).first_or_404()
    form = ConstructionForm(obj=construction)
    if form.validate_on_submit():
        construction_image = form.construction_image.data
        if construction_image is not None and construction.construction_image != construction_image:
            construction_image = photos.save(form.construction_image.data)
            form.construction_image.data = construction_image
        form.populate_obj(construction)
        db.session.commit()
        flash('Construction  edited ', 'success')
        return redirect(url_for('main.view_construction_materials',
                                construction_name=construction.construction_types.construction_name))
    return render_template('admin/edit-construction-materials.html', construction=construction, form=form,
                           title='edit construction_materials')


@main.route('/edit_consultancy_services/<id>', methods=['GET', 'POST'])
@login_required
def edit_consultancy_services(id):
    if current_user.role.permissions.write_consultancy != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    consultancy = Consultancy.query.filter_by(id=id).first_or_404()
    form = ConsultancyForm(obj=consultancy)
    if form.validate_on_submit():
        consultancy_image = form.image.data
        if consultancy_image is not None and consultancy.image != consultancy_image:
            consultancy_image = photos.save(form.image.data)
            form.image.data = consultancy_image
        form.populate_obj(consultancy)
        db.session.commit()
        flash('Consultancy  edited ', 'success')
        return redirect(
            url_for('main.view_consultancy_services', consultancy_name=consultancy.consultancy_types.consultancy_name))
    return render_template('admin/edit-consultancy-services.html', consultancy=consultancy, form=form,
                           title='edit consultancy_services')


@main.route('/edit_latest_house_designs/<id>', methods=['GET', 'POST'])
@login_required
def edit_latest_house_designs(id):
    if current_user.role.permissions.write_house_design != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    house_design = HouseDesign.query.filter_by(id=id).first_or_404()
    form = HouseDesignForm(obj=house_design)
    if form.validate_on_submit():
        house_design_image = form.house_design_image.data
        if house_design_image is not None and house_design.house_design_image != house_design_image:
            house_design_image = photos.save(form.house_design_image.data)
            form.house_design_image.data = house_design_image
        form.populate_obj(house_design)
        db.session.commit()
        flash('House Design  edited ', 'success')
        return redirect(url_for('main.view_latest_house_designs_type',
                                house_design_name=house_design.house_design_types.house_design_name))
    return render_template('admin/edit-latest-house-designs.html', house_design=house_design, form=form,
                           title='edit latest_house_designs')

@main.route('/edit_tenant/<id>', methods=['GET', 'POST'])
@login_required
def edit_tenant(id):
    if current_user.role.permissions.write_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    tenant = Tenant.query.filter_by(id=id).first_or_404()
    g.house_no = tenant.house_no
    g.house_id = tenant.property_id
    form = EditTenantForm(obj=tenant)
    form.house.choices = [(row.id, row.property_name) for row in Property.query.filter(Property.property_vacancy > 0).filter(Property.author==current_user).filter(Property.property_mode=='rent').order_by(Property.created_at.desc()).all()]
    if form.validate_on_submit():
        tenant_image = form.image.data
        if tenant_image is not None and tenant.image != tenant_image:
            tenant_image = photos.save(form.image.data)
            tenant.image = tenant_image
        house = Property.query.filter_by(id=form.house.data).first_or_404()
        tenant.first_name=form.first_name.data
        tenant.last_name=form.last_name.data
        tenant.house_no=form.house_no.data
        tenant.dob=form.dob.data
        tenant.gender=form.gender.data
        tenant.county=form.county.data
        tenant.town=form.town.data
        tenant.phone_number=form.phone_number.data
        tenant.house=house
        db.session.commit()
        flash('Tenant  edited ', 'success')
        return redirect(url_for('main.tenants'))
    return render_template('admin/edit-tenant.html', form=form,tenant=tenant,
                           title='edit tenant')

@main.route('/edit_payment/<id>', methods=['GET', 'POST'])
@login_required
def edit_payment(id):
    if current_user.role.permissions.write_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payment = Payment.query.filter_by(id=id).first_or_404()
    form = PaymentForm(obj=payment)
    form.tenant_id.choices = [(row.id, row.email) for row in Tenant.query.filter_by(author=current_user).order_by(Tenant.created_at.desc()).all()]
    if form.validate_on_submit():
        print(form.tenant_id.data)
        tenant = Tenant.query.filter_by(id=form.tenant_id.data).first_or_404()
        if payment.amount != form.amount.data:
            change=form.amount.data-payment.amount
            tenant.rent_due =(tenant.rent_due-change )
        form.populate_obj(payment)
        db.session.commit()
        flash('Payment  edited ', 'success')
        return redirect(url_for('main.payments'))
    return render_template('admin/edit-payment.html', form=form,
                           title='edit payment')

@main.route('/edit_financing/<financing_id>', methods=['GET', 'POST'])
@login_required
def edit_financing(financing_id):
    if current_user.role.permissions.write_financing != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    financing = Financing.query.filter_by(id=financing_id).first_or_404()
    form = FinancingForm()
    if form.validate_on_submit():
        form.populate_obj(financing)
        db.session.commit()
        return jsonify({'status': 1, 'message': 'finance edited successfully'})
    if request.method == 'GET':
        data = {'financing_name': financing.financing_name, 'financing_description': financing.financing_description,
                'id': financing.id}
        return jsonify({'status': 1, 'data': data})
    return jsonify(data=form.errors)


@main.route('/edit_projects/<project_id>', methods=['GET', 'POST'])
@login_required
def edit_projects(project_id):
    if current_user.role.permissions.write_projects != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    project = Project.query.filter_by(id=project_id).first_or_404()
    form = ProjectForm()
    if form.validate_on_submit():
        project_image = form.project_image.data
        if project_image is not None and project.project_image != project_image:
            project_image = photos.save(form.project_image.data)
            form.project_image.data = project_image
        else:
            form.project_image.data = project.project_image
        form.populate_obj(project)
        db.session.commit()
        return jsonify({'status': 1, 'message': 'project edited successfully'})
    if request.method == 'GET':
        return jsonify({'status': 1, 'data': project.to_dict_project()})
    return jsonify(data=form.errors)


@main.route('/edit_property_features/<property_id>', methods=['GET', 'POST'])
@login_required
def edit_property_features(property_id):
    propertyname = Property.query.filter(id=property_id).first_or_404
    form = PropertyFeatureForm()
    if form.validate_on_submit():
        propertyfeature = PropertyFeature(property_id=propertyname.id, feature_name=form.feature_name.data,
                                          feature_icon=form.feature_icon.data)
        db.session.add(propertyfeature)
        db.session.commit()
        flash('Property feature edited successfully', 'success')
    return render_template('admin/edit-property-features.html', title='edit property_features')


@main.route('/edit_property_type/<id>', methods=['GET', 'POST'])
@login_required
def edit_property_type(id):
    if current_user.role.permissions.write_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    property_type = PropertyType.query.filter_by(id=id).first_or_404()
    form = PropertyTypeForm(obj=property_type)
    if form.validate_on_submit():
        property_image = form.property_image.data
        if property_image is not None and property_type.property_image != property_image:
            property_image = photos.save(form.property_image.data)
            property_type.property_image = property_image
        property_type.property_name = form.property_name.data
        db.session.commit()
        flash('Property Type edited ', 'success')
        return redirect(url_for('main.view_property_type'))
    return render_template('admin/edit-property-type.html', property_type=property_type, form=form,
                           title='edit property type')


@main.route('/edit_labour_type/<id>', methods=['GET', 'POST'])
@login_required
def edit_labour_type(id):
    if current_user.role.permissions.write_labour != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    labour_type = LabourType.query.filter_by(id=id).first_or_404()
    form = LabourTypeForm(obj=labour_type)
    if form.validate_on_submit():
        labour_image = form.labour_image.data
        if labour_image is not None and labour_type.labour_image != labour_image:
            labour_image = photos.save(form.labour_image.data)
            labour_type.labour_image = labour_image
        labour_type.labour_name = form.labour_name.data
        db.session.commit()
        flash('Labour Type edited ', 'success')
        return redirect(url_for('main.view_labour_type'))
    return render_template('admin/edit-labour-type.html', labour_type=labour_type, form=form, title='edit labour type')


@main.route('/edit_equipment_type/<id>', methods=['GET', 'POST'])
@login_required
def edit_equipment_type(id):
    if current_user.role.permissions.write_equipment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    equipment_type = EquipmentType.query.filter_by(id=id).first_or_404()
    form = EquipmentTypeForm(obj=equipment_type)
    if form.validate_on_submit():
        equipment_image = form.equipment_image.data
        if equipment_image is not None and equipment_type.equipment_image != equipment_image:
            equipment_image = photos.save(form.equipment_image.data)
            equipment_type.equipment_image = equipment_image
        equipment_type.equipment_name = form.equipment_name.data
        db.session.commit()
        flash('Equipment Type edited ', 'success')
        return redirect(url_for('main.view_equipment_type'))
    return render_template('admin/edit-equipment-type.html', equipment_type=equipment_type, form=form,
                           title='edit equipment type')


@main.route('/edit_construction_materials_type/<id>', methods=['GET', 'POST'])
@login_required
def edit_construction_materials_type(id):
    if current_user.role.permissions.write_construction != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    construction_type = ConstructionType.query.filter_by(id=id).first_or_404()
    form = ConstructionTypeForm(obj=construction_type)
    if form.validate_on_submit():
        construction_image = form.construction_image.data
        if construction_image is not None and construction_type.construction_image != construction_image:
            construction_image = photos.save(form.construction_image.data)
            construction_type.construction_image = construction_image
        construction_type.construction_name = form.construction_name.data
        db.session.commit()
        flash('Construction Type edited ', 'success')
        return redirect(url_for('main.view_construction_materials_type'))
    return render_template('admin/edit-construction-materials-type.html', construction_type=construction_type, form=form,
                           title='edit construction_materials type')


@main.route('/edit_consultancy_services_type/<id>', methods=['GET', 'POST'])
@login_required
def edit_consultancy_services_type(id):
    if current_user.role.permissions.write_consultancy != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    consultancy_type = ConsultancyType.query.filter_by(id=id).first_or_404()
    form = ConsultancyTypeForm(obj=consultancy_type)
    if form.validate_on_submit():
        consultancy_image = form.consultancy_image.data
        if consultancy_image is not None and consultancy_type.consultancy_image != consultancy_image:
            consultancy_image = photos.save(form.consultancy_image.data)
            consultancy_type.consultancy_image = consultancy_image
        consultancy_type.consultancy_name = form.consultancy_name.data
        db.session.commit()
        flash('Consultancy Type edited ', 'success')
        return redirect(url_for('main.view_consultancy_services_type'))
    return render_template('admin/edit-consultancy-services-type.html', consultancy_type=consultancy_type, form=form,
                           title='edit consultancy_services type')


@main.route('/edit_latest_house_designs_type/<id>', methods=['GET', 'POST'])
@login_required
def edit_latest_house_designs_type(id):
    if current_user.role.permissions.write_house_design != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    house_design_type = HouseDesignType.query.filter_by(id=id).first_or_404()
    form = HouseDesignTypeForm(obj=house_design_type)
    if form.validate_on_submit():
        house_design_image = form.house_design_image.data
        if house_design_image is not None and house_design_type.house_design_image != house_design_image:
            house_design_image = photos.save(form.house_design_image.data)
            house_design_type.house_design_image = house_design_image
        house_design_type.house_design_name = form.house_design_name.data
        db.session.commit()
        flash('HouseDesign Type edited ', 'success')
        return redirect(url_for('main.view_latest_house_designs_typey_type'))
    return render_template('admin/edit-latest-house-designs-type.html', house_design_type=house_design_type, form=form,
                           title='edit latest_house_designs type')


@main.route('/view_property/<id>', methods=['GET', 'POST'])
@login_required
def view_property(id):
    if current_user.role.permissions.read_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Property'
    page = request.args.get('page', 1, type=int)
    propertylist = Property.query.filter_by(property_type_id=id).order_by(
        Property.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    if current_user.role.role != 'admin':
        propertylist = Property.query.filter_by(property_type_id=id,author=current_user).order_by(
            Property.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('main.view_property', id=id, page=propertylist.next_num) if propertylist.has_next else None
    prev_url = url_for('main.view_property', id=id, page=propertylist.prev_num) if propertylist.has_prev else None
    return render_template('admin/view-property.html', propertylist=propertylist.items, next_url=next_url, prev_url=prev_url,
                           category=id, title='view property')


@main.route('/view_labour/<labour_name>', methods=['GET', 'POST'])
@login_required
def view_labour(labour_name):
    if current_user.role.permissions.read_labour != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Labour'
    page = request.args.get('page', 1, type=int)
    labour_type = LabourType.query.filter_by(labour_name=labour_name).first_or_404()
    labourlist = Labour.query.filter_by(labour_type_id=labour_type.id).order_by(Labour.created_at.desc()).paginate(page,
                                                                                                                   current_app.config[
                                                                                                                       'POSTS_PER_PAGE'],
                                                                                                                   False)
    next_url = url_for('main.view_labour', labour_name=labour_name, page=labourlist.next_num) if labourlist.has_next else None
    prev_url = url_for('main.view_labour', labour_name=labour_name, page=labourlist.prev_num) if labourlist.has_prev else None
    return render_template('admin/view-labour.html', category=labour_name, labourlist=labourlist.items, next_url=next_url,
                           prev_url=prev_url, title='view labour')


@main.route('/view_equipment/<equipment_name>', methods=['GET', 'POST'])
@login_required
def view_equipment(equipment_name):
    if current_user.role.permissions.read_equipment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Equipment'
    page = request.args.get('page', 1, type=int)
    equipment_type = EquipmentType.query.filter_by(equipment_name=equipment_name).first_or_404()
    equipmentlist = Equipment.query.filter_by(equipment_type_id=equipment_type.id).order_by(
        Equipment.created_at.desc()).paginate(page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_equipment', equipment_name=equipment_name, page=equipmentlist.next_num) if equipmentlist.has_next else None
    prev_url = url_for('main.view_equipment', equipment_name=equipment_name, page=equipmentlist.prev_num) if equipmentlist.has_prev else None
    return render_template('admin/view-equipment.html', category=equipment_name, equipmentlist=equipmentlist.items,
                           next_url=next_url, prev_url=prev_url, title='view equipment')


@main.route('/view_construction_materials/<construction_name>', methods=['GET', 'POST'])
@login_required
def view_construction_materials(construction_name):
    if current_user.role.permissions.read_construction != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Construction'
    page = request.args.get('page', 1, type=int)
    construction_type = ConstructionType.query.filter_by(construction_name=construction_name).first_or_404()
    constructionmaterialslist = Construction.query.filter_by(construction_types_id=construction_type.id).order_by(
        Construction.created_at.desc()).paginate(page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_construction_materials', construction_name=construction_name, page=constructionmaterialslist.next_num) if constructionmaterialslist.has_next else None
    prev_url = url_for('main.view_construction_materials', construction_name=construction_name, page=constructionmaterialslist.prev_num) if constructionmaterialslist.has_prev else None
    return render_template('admin/view-construction-materials.html',
                           constructionmaterialslist=constructionmaterialslist.items, next_url=next_url,
                           prev_url=prev_url, category=construction_name, title='view construction materials')


@main.route('/view_consultancy_services/<consultancy_name>', methods=['GET', 'POST'])
@login_required
def view_consultancy_services(consultancy_name):
    if current_user.role.permissions.read_consultancy != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Consultancy'
    consultancy_type = ConsultancyType.query.filter_by(consultancy_name=consultancy_name).first_or_404()
    page = request.args.get('page', 1, type=int)
    consultancylist = Consultancy.query.filter_by(consultancy_type_id=consultancy_type.id).order_by(
        Consultancy.created_at.desc()).paginate(page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_consultancy_services', consultancy_name=consultancy_name, page=consultancylist.next_num) if consultancylist.has_next else None
    prev_url = url_for('main.view_consultancy_services', consultancy_name=consultancy_name, page=consultancylist.prev_num) if consultancylist.has_prev else None
    return render_template('admin/view-consultancy-services.html', consultancylist=consultancylist.items, next_url=next_url,
                           prev_url=prev_url, category=consultancy_name, title='view consultancy services')


@main.route('/view_latest_house_designs/<house_design_name>', methods=['GET', 'POST'])
@login_required
def view_latest_house_designs(house_design_name):
    if current_user.role.permissions.read_house_design != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'HouseDesign'
    house_design_type = HouseDesignType.query.filter_by(house_design_name=house_design_name).first_or_404()
    page = request.args.get('page', 1, type=int)
    housedesignlist = HouseDesign.query.filter_by(house_design_id=house_design_type.id).order_by(
        HouseDesign.created_at.desc()).paginate(page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.view_latest_house_designs',  house_design_name=house_design_name, page=housedesignlist.next_num) if housedesignlist.has_next else None
    prev_url = url_for('main.view_latest_house_designs', house_design_name=house_design_name, page=housedesignlist.prev_num) if housedesignlist.has_prev else None
    return render_template('admin/view-latest-house-designs.html', housedesignlist=housedesignlist.items, next_url=next_url,
                           prev_url=prev_url, category=house_design_name, title='view latest house designs')


@main.route('/view_financing/<id>', methods=['GET', 'POST'])
@login_required
def view_financing(id):
    if current_user.role.permissions.read_financing != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    financing = Financing.query.filter_by(id=id).first_or_404()
    return render_template('admin/view-financing.html', financing=financing, title='view financing')


@main.route('/view_projects/<id>', methods=['GET', 'POST'])
@login_required
def view_projects(id):
    if current_user.role.permissions.read_projects != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    project = Project.query.filter_by(id=id).first_or_404()
    return render_template('admin/view-projects.html', project=project, title='view project')


@main.route('/view_individual_property/<id>')
@login_required
def view_individual_property(id):
    if current_user.role.permissions.read_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    property = Property.query.filter_by(id=id).first_or_404()
    return render_template('admin/view-individual-property.html', property=property, title='view property')


@main.route('/getconstruction')
@login_required
def getconstruction():
    id = request.args.get('id')
    construction = Construction.query.filter_by(id=id).first_or_404()
    data = {'name': construction.construction_name, 'image': construction.construction_image,
            'price': construction.construction_price,
            'location': construction.construction_location}
    return jsonify(data=data)


@main.route('/getequipment')
@login_required
def getequipment():
    id = request.args.get('id')
    equipment = Equipment.query.filter_by(id=id).first_or_404()
    data = {'name': equipment.equipment_name, 'image': equipment.equipment_image,
            'price': equipment.equipment_price,
            'location': equipment.equipment_location}
    return jsonify(data=data)


@main.route('/gethouse_design')
@login_required
def gethouse_design():
    id = request.args.get('id')
    house_design = HouseDesign.query.filter_by(id=id).first_or_404()
    data = {'name': house_design.house_design_name, 'image': house_design.house_design_image}
    return jsonify(data=data)


@main.route('/getconsultancy')
@login_required
def getconsultancy():
    id = request.args.get('id')
    consultancy = Consultancy.query.filter_by(id=id).first_or_404()
    data = {'first_name': consultancy.first_name, 'last_name': consultancy.last_name,
            'image': consultancy.image,
            'county': consultancy.county,
            'town': consultancy.town,
            'dob': consultancy.dob,
            'gender': consultancy.gender,
            'phone_number': consultancy.phone_number,
            'email': consultancy.email}
    return jsonify(data=data)


@main.route('/getlabour')
@login_required
def getlabour():
    id = request.args.get('id')
    labour = Labour.query.filter_by(id=id).first_or_404()
    data = {'first_name': labour.first_name, 'last_name': labour.last_name,
            'image': labour.image,
            'county': labour.county,
            'town': labour.town,
            'dob': labour.dob,
            'gender': labour.gender,
            'phone_number': labour.phone_number,
            'email': labour.email}
    return jsonify(data=data)


@main.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    id = request.form['id']
    tablename = request.form['table']
    if tablename == 'PropertyType':
        table = PropertyType.query.filter_by(id=id).first_or_404()
    else:
        if tablename == 'ConstructionType':
            table = ConstructionType.query.filter_by(id=id).first_or_404()
        else:
            if tablename == 'ConsultancyType':
                table = ConsultancyType.query.filter_by(id=id).first_or_404()
            else:
                if tablename == 'EquipmentType':
                    table = EquipmentType.query.filter_by(id=id).first_or_404()
                else:
                    if tablename == 'LabourType':
                        table = LabourType.query.filter_by(id=id).first_or_404()
                    else:
                        if tablename == 'HouseDesignType':
                            table = HouseDesignType.query.filter_by(id=id).first_or_404()
                        else:
                            if tablename == 'Property':
                                table = Property.query.filter_by(id=id).first_or_404()
                            else:
                                if tablename == 'Construction':
                                    table = Construction.query.filter_by(id=id).first_or_404()
                                else:
                                    if tablename == 'Consultancy':
                                        table = Consultancy.query.filter_by(id=id).first_or_404()
                                    else:
                                        if tablename == 'Equipment':
                                            table = Equipment.query.filter_by(id=id).first_or_404()
                                        else:
                                            if tablename == 'Labour':
                                                table = Labour.query.filter_by(id=id).first_or_404()
                                            else:
                                                if tablename == 'HouseDesign':
                                                    table = HouseDesign.query.filter_by(id=id).first_or_404()
                                                else:
                                                    if tablename == 'Project':
                                                        table = Project.query.filter_by(id=id).first_or_404()
                                                    else:
                                                        if tablename == 'Financing':
                                                            table = Financing.query.filter_by(id=id).first_or_404()
                                                        else:
                                                            if tablename == 'User':
                                                                table = User.query.filter_by(id=id).first_or_404()
                                                            else:
                                                                if tablename == 'Role':
                                                                    table = Role.query.filter_by(id=id).first_or_404()
                                                                else:
                                                                    if tablename == 'Tenant':
                                                                        table = Tenant.query.filter_by(id=id).first_or_404()
                                                                    else:
                                                                        return jsonify({'status': 0})
    db.session.delete(table)
    db.session.commit()
    return jsonify({'status': 1})


@main.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.index'))
    search = g.search_form.q.data
    tablename = g.search_form.table.data
    if tablename == 'PropertyType':
        table = PropertyType.query.filter(PropertyType.property_name.like('%' + search + '%')).all()
        return render_template('admin/property.html', property_types=table, title='property Types')
    if tablename == 'ConstructionType':
        table = ConstructionType.query.filter(ConstructionType.construction_name.like('%' + search + '%')).all()
        return render_template('admin/construction-materials.html', construction_types=table, title='construction Types')
    if tablename == 'ConsultancyType':
        table = ConsultancyType.query.filter(ConsultancyType.consultancy_name.like('%' + search + '%')).all()
        return render_template('admin/consultancy-services.html', consultancy_types=table, title='consultancy Types')
    if tablename == 'EquipmentType':
        table = EquipmentType.query.filter(EquipmentType.equipment_name.like('%' + search + '%')).all()
        return render_template('admin/equipment.html', equipment_types=table, title='property Types')
    if tablename == 'LabourType':
        table = LabourType.query.filter(LabourType.labour_name.like('%' + search + '%')).all()
        return render_template('admin/labour.html', labour_types=table, title='property Types')
    if tablename == 'HouseDesignType':
        table = HouseDesignType.query.filter(HouseDesignType.house_design_name.like('%' + search + '%')).all()
        return render_template('admin/latest-house-designs.html', house_designs_types=table, title='property Types')
    if tablename == 'Property':
        table = Property.query.filter(Property.property_name.like('%' + search + '%')).all()
        return render_template('admin/view-property.html', propertylist=table, title='property')
    if tablename == 'Construction':
        table = Construction.query.filter(Construction.construction_name.like('%' + search + '%')).all()
        return render_template('admin/view-construction-materials.html', constructionmaterialslist=table,
                               title='construction')
    if tablename == 'Consultancy':
        table = Consultancy.query.filter(Consultancy.first_name.like('%' + search + '%')).all()
        return render_template('admin/view-consultancy-services.html', consultancylist=table, title='consultancy')
    if tablename == 'Equipment':
        table = Equipment.query.filter(Equipment.equipment_name.like('%' + search + '%')).all()
        return render_template('admin/view-equipment.html', equipmentlist=table, title='equipment')
    if tablename == 'Labour':
        table = Labour.query.filter(Labour.first_name.like('%' + search + '%')).all()
        return render_template('admin/view-labour.html', labourlist=table, title='labour')
    if tablename == 'HouseDesign':
        table = HouseDesign.query.filter(HouseDesign.house_design_name.like('%' + search + '%')).all()
        return render_template('admin/view-latest-house-designs.html', housedesignlist=table, title='house design')
    if tablename == 'Project':
        table = Project.query.filter(Project.project_name.like('%' + search + '%')).all()
        form=ProjectForm()
        return render_template('admin/projects.html',form=form, projects=table, title='projects')
    if tablename == 'Financing':
        table = Financing.query.filter(Financing.financing_name.like('%' + search + '%')).all()
        form=FinancingForm()
        return render_template('admin/financing.html',form=form, financing=table, title='financing')
    return render_template('errors/404.html', title=_('Not found'))
